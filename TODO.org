* DONE What's Literate Programming?
CLOSED: [2021-10-06 Wed 21:52]

We'll try with Emacs, with org-mode and babel. All of this comes with
standard Emacs these days.

* DONE Just one file
CLOSED: [2021-10-06 Wed 21:52]
A file with some commentary. A few chunks.

    - ~<s <tab> sh~ to create source chunk
    - ~C-c '~ to edit source

* DONE Include one chunk in another.
CLOSED: [2021-10-06 Wed 21:52]
Use noweb links.

* DONE Changing chunk delimiters
CLOSED: [2021-10-06 Wed 21:52]

* DONE Multiple files
CLOSED: [2021-10-06 Wed 21:53]
Sometimes, what's conceptually one change will involve editing multiple files.

* TODO Cheat sheet

** TODO org-mode headers

** TODO ~#+begin_src~ headers

*** ~:noweb~
https://www.gnu.org/software/emacs/manual/html_node/org/Noweb-Reference-Syntax.html

    - no: No expansion ever
    - yes: Expand 

| *Value*        | eval | tangle | export  |
| ~no~           | no   | no     | no      |
| ~yes~          | yes  | yes    | yes     |
| ~tangle~       | no   | yes    | no      |
| ~no-export~    | yes  | yes    | no      |
| ~strip-export~ | yes  | yes    | special |
| ~eval~         | yes  | no     | no      |


** TODO Interesting local variables

* TODO Makefile

** TODO Tangle (build documentation)

** TODO Export files

** TODO Clean up

There should be a way to automatically generate a list of exported files. It might involve ~org-map-entries~.

See also
https://emacs.stackexchange.com/questions/47287/loop-through-top-level-headings-in-emacs-org-mode
and
https://orgmode.org/manual/Matching-tags-and-properties.html#Matching-tags-and-properties
for query syntax.

#+begin_src elisp
  (org-block-map
   (lambda nil)
   (message "Looking at block"))
#+end_src

The function doesn't take any arguments, so I guess this updates the position, and each time the function is called, we're looking at a source block (or some kind of block, at least.

Maybe use ~org-babel-get-src-block-info~ to get information about the current block.
That returns a list of the form
: (language body arguments switches name start coderef)

~arguments~ is a list of tuples. One of them is of the form

: (:tangle . "lesson1.org")

Extract that and add it to the list of generated files.
* TODO Move tutorial.css to a separate source file
Make a ~tutorial.org~ to maintain the CSS file.
